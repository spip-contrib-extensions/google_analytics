<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'googleanalytics_description' => 'Adds the Google Analytics script in public space.',
	'googleanalytics_slogan' => 'Adds Google Analytics',
);
?>
