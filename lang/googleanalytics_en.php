<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_descriptif' => 'Enter your Google Analytics ID.',
	'cfg_titre' => 'Google Analytics',

	// E
	'explication_id_google' => 'If "_" or empty, removes the functionality (empty revert to default).',

	// L
	'label_id_google' => 'Your Google Analytics ID like "G-12345"',
	'label_ga_universal' => 'Use <a href="https://support.google.com/analytics/answer/2790010">Google Analytics Universal</a>',
);
?>
