<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}


/**
 * Dans SPIP 3 on utilise insert_head_css qui est safe
 * et on insere avant les CSS, pour ne pas bloquer celles-ci
 * (qui sont bloquees par du js inline)
 *
 * @param $flux
 * @return string
 */
function googleanalytics_insert_head_css($flux) {
	return $flux . googleanalytics_snippet();
}

/**
 * Morceau de code a inserer dans la page pour traquer avec GA
 * @return string
 */
function googleanalytics_snippet() {
	include_spip('inc/config');
	$id_google = lire_config('googleanalytics/idGoogle');
	$cookiebar = (isset($_COOKIE["cb-enabled"]) ? $_COOKIE["cb-enabled"] : '');
	$displayCookieConsent = ((isset($_COOKIE["displayCookieConsent"]) and strlen($_COOKIE["displayCookieConsent"])) ? $_COOKIE["displayCookieConsent"] : 'y');
	if ($id_google
		and $id_google !== '_'
		and (strncmp($id_google, "UA-xxx", 6) !== 0)
		and (strncmp($id_google, "G-xxx", 5) !== 0)
		and $cookiebar !== 'declined'
		and $displayCookieConsent === 'y') {

		// eviter d'envoyer des caractères innatendus
		$id_google = htmlspecialchars($id_google);

		// cas legacy des anciens Universal Analytics
		// @deprecated au 2023-07-01
		if (strpos($id_google, "UA-") === 0) {
			$js = <<<uatag
<script type='text/javascript'>/*<![CDATA[*/
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })
(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '{$id_google}');
ga('send', 'pageview');
/*]]>*/</script>
uatag;

		} else {
			$id_google = str_replace("'", "", $id_google);
			$js = <<<gtag
<script async src="https://www.googletagmanager.com/gtag/js?id={$id_google}"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){window.dataLayer.push(arguments);}gtag('js', new Date());gtag('config', '{$id_google}');</script>
gtag;
		}

		return $js;
	}
	return "";
}
